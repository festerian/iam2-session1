#!/src/bash
if [ $# -ne 1 ]; then
	echo "Script requires a single argument representing a filename."
else
	echo "Currently testing $1!"
	
	if ! test -f $1 ; then
		echo "This file does not exist!"
	fi
fi
