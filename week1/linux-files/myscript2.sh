#!/src/bash
if [ ! "$1" ]; then
	echo "Script requires at last one argument representing a filename."
else
	for testarg in $@; do
		echo "Currently testing $testarg!"
		if ! test -f $testarg ; then
			echo "$testarg does not exist!"
		else 
			echo "File inspected by $0" >> $testarg
			if [ -s $testarg ]; then
				echo "$testarg is empty - removing it"
				rm $testarg
			fi
		fi
	done
fi
