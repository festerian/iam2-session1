#!/src/bash

### Count all img in a directory structure.

if [ ! "$1" ]; then
	echo "No basedir specified; running in ./ - specify a different directory if required!"
	for file in `ls -lR ./ | grep -P "^\-" | awk {'print $10'}`; do
		if file -s $file | grep -P "\bimage\b"; then
			echo "$file is an image"
		fi
	done
else
	echo "Starting basedir in $1"
	for file in `ls -lR "$1" | grep -P "^\-" | awk {'print $10'}`; do
		if file -s $file | grep -P "\bimage\b"; then
			echo "$file is an image"
		fi	
	done
fi


