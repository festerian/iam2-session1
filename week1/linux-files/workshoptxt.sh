#!/src/bash

### Count all files ending with .txt in a directory structure.


if [ ! "$1" ]; then
	echo "No basedir specified; running in ./"
	ls -alR ./ | grep -Pc "\.(txt)"
else
	ls -alR "$1" | grep -Pc "\.(txt)"
fi

